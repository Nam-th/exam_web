﻿using Microsoft.EntityFrameworkCore;

namespace exam_ltweb.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }


    }
}
